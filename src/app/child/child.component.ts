import { Component, OnDestroy, OnInit, OnChanges, Input, SimpleChanges, ViewChild, ElementRef, AfterViewInit } from '@angular/core';

@Component({
  selector: 'app-child',
  templateUrl: './child.component.html',
  styleUrls: ['./child.component.scss']
})
export class ChildComponent implements OnInit, OnDestroy, OnChanges, AfterViewInit{

  @ViewChild('myelement') myelement!: ElementRef;

  counter = 0;
  interval:any;
  @Input() channelName = '';

  constructor(){
    console.log('Child constructor is called.')
  }
  
  ngOnInit(): void {
    console.log('Child onInit is called.');
    /*   this.interval = setInterval( () =>{
    this.counter = this.counter +1;
    console.log(this.counter)
  }, 1000); */

  }

  ngOnDestroy(): void {
    console.log('Child onDestroy is called.')
    clearInterval(this.interval)
  }
  
  ngOnChanges(changes: SimpleChanges){
    console.log('Child onChanges is called.')
    console.log(changes)
  }
  
  
  ngAfterViewInit(): void {
    console.log(this.myelement)
  }
}
