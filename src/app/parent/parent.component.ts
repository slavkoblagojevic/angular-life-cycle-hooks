import { Component, OnInit, OnChanges } from '@angular/core';

@Component({
  selector: 'app-parent',
  templateUrl: './parent.component.html',
  styleUrls: ['./parent.component.scss']
})
export class ParentComponent implements OnInit, OnChanges {

  showChild:boolean = true;
  inputText= '';

  constructor(){
    console.log('Parent constructor is called.')
  }
  
  ngOnInit(): void {
    console.log('Parent onInit is called.')
  }

  toggleChild(){
    this.showChild = !this.showChild;
  }

  ngOnChanges(){
    console.log('Parent onChanges is called.')
  }

}
